package CucumberImplementation;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@CucumberOptions(features = "./src/test/java/CucumberImplementation/scratch.feature",
  glue = {"CucumberImplementation"},
        plugin = {
        "pretty","html:CucumberReports/LoginBDD.html"
        },
        monochrome = true
)
@RunWith(Cucumber.class)
public class Runner {

}
