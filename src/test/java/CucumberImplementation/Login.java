package CucumberImplementation;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login {
    WebDriver driver;

    @Given("Open Browser and navigate to Login Page")
    public void open_browser_and_navigate_to_login_page()
    {
        System.setProperty("webdriver.chrome.driver","./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
    }
    @When("Enter Email and Password")
    public void enter_email_and_password()
    {
        WebElement Email = driver.findElement(By.id("username"));
        Email.clear();
        Email.sendKeys("b@gmail.com");
        WebElement Pass = driver.findElement(By.id("password"));
        Pass.clear();
        Pass.sendKeys("12");

        WebElement LoginBtn = driver.findElement(By.xpath("//*[@id=\"signin\"]/div/div/div/div/div[2]/form/div/div[4]/button"));
        LoginBtn.click();

        String ExpectedTitle = "Account Login";
        String ActualTitle = driver.getTitle();

        if(ExpectedTitle.equals(ActualTitle))
        {
            System.out.println("Title Verification Passed");
        }
        else {
            System.out.println("Title Verification Failed"+"Actual Title is: "+ActualTitle);
        }

    }
    @Then("Login Unsucessful and close test")
    public void Login_Unsucessful_and_close_test()
    {
        driver.close();
    }



}
