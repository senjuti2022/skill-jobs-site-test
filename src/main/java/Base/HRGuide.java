package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HRGuide {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"__layout\"]/div/div/div[1]/div/div/div[4]/a")).click();
    }
}
