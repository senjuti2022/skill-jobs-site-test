package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SkillAssessment {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"navbarNav\"]/ul/li[3]/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"govt_job_browse\"]/div/div[1]/div[1]/div/div/div[1]/input")).sendKeys("English");
        driver.findElement(By.className("searchButton")).click();
    }
}
