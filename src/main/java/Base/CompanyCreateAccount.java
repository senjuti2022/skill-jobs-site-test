package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CompanyCreateAccount {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","./src/main/resources/geckodr.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.findElement(By.xpath("//*[@id=\"navbarDropdownMenuLink\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"navbarNav\"]/ul/li[4]/ul/li[2]/p[3]/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys("k@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("123456");
        driver.findElement(By.className("confirmPassHasNoError")).sendKeys("123456");
        driver.findElement(By.xpath("//*[@id=\"signup\"]/div/div/div/div/div[2]/div[6]/button")).click();
        driver.findElement(By.xpath("/html/body/div[2]/div/div[6]/button[1]")).click();
    }
}
