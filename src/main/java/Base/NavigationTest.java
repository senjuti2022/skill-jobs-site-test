package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class NavigationTest {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver", "./src/main/resources/geckodr.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.google.com/");
        driver.get("https://mainsite.skill.jobs/");
        driver.navigate().back();
        driver.navigate().forward();
    }
}
