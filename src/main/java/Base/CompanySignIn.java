package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CompanySignIn {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.findElement(By.id("navbarDropdownMenuLink")).click();
        driver.findElement(By.xpath("//*[@id=\"navbarNav\"]/ul/li[4]/ul/li[2]/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys("rfl20");
        driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("admin123");
        driver.findElement(By.xpath("//*[@id=\"signin\"]/div/div/div/div/div[2]/form/div/div[4]/button")).click();
}
}
