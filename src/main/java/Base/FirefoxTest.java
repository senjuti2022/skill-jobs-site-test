package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxTest {
    public static void main(String[] args) {
        System.setProperty("webdriver.gecko.driver","./src/main/resources/geckodr.exe");
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.google.com/");
        driver.get("https://mainsite.skill.jobs/");
        driver.navigate().back();
        driver.navigate().forward();
        //driver.navigate().to();
       // driver.findElement(By.id("navbarDropdownMenuLink")).click();
        //driver.findElement(By.xpath("//*[@id=\"navbarNav\"]/ul/li[4]/ul/li[1]/p[3]/span/a/span")).click();
        //driver.findElement(By.id("email")).sendKeys("z@gmail.com");
        //driver.findElement(By.id("password")).sendKeys("123456");
        //driver.findElement(By.className("confirmPassHasNoError")).sendKeys("123456");
       // driver.findElement(By.xpath("//*[@id=\"signup\"]/div/div/div/div/div[2]/div[5]/button")).click();
        driver.close();
    }
}
