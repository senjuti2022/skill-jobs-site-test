package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class GovtJobs {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.findElement(By.xpath("//*[@id=\"__layout\"]/div/div/div[1]/div/div/div[1]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"govt_job_browse\"]/div/div[1]/div/div/div[1]/form/div/input")).sendKeys("SUST");
        driver.findElement(By.xpath("//*[@id=\"govt_job_browse\"]/div/div[1]/div/div/div[1]/form/button")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.close();
    }
}
