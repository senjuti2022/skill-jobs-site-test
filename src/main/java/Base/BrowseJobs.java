package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BrowseJobs {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","./src/main/resources/cdriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mainsite.skill.jobs/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"navbarNav\"]/ul/li[1]/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"browse_job\"]/div/div/div[2]/div/div[1]/form/div[1]/input")).sendKeys("Software Developer");
        driver.findElement(By.xpath("//*[@id=\"browse_job\"]/div/div/div[2]/div/div[1]/form/div[2]/input")).sendKeys("Dhaka");
        driver.findElement(By.xpath("//*[@id=\"browse_job\"]/div/div/div[2]/div/div[1]/form/button[1]")).click();
        driver.findElement(By.xpath("/html")).click();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.close();
    }
}
